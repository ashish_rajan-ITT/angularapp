import { Component, OnInit } from '@angular/core';
import {PostService} from '../../../../providers/post.service';
import {Post} from '../../models/post';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  posts: Post[];

  constructor(private postService: PostService) { }

  ngOnInit(): void {
    this.posts = [];
  }
  getAllPosts() {
    this.postService.getAllPosts().subscribe({
      next: res => this.posts = res,
      error: err => console.log('Error Occured'),
      complete: () => console.log('Got all the data')
    });
  }



}
