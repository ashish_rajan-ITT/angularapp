import { TestBed } from '@angular/core/testing';

import { PostService } from './post.service';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {AppComponent} from '../app.component';
import {Post} from '../modules/user/models/post';
import {of} from 'rxjs';
import {fakePosts} from './testData/postServiceTestData';

fdescribe('PostService', () => {
  let service: PostService;
  let httpClientSpy: { get: jasmine.Spy };


  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
    TestBed.configureTestingModule({

      providers: [
        { provide: HttpClient, useValue: httpClientSpy }
      ]
    });
    service = TestBed.inject(PostService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('should return posts on calling fet posts', () => {
    const expectedPosts: Post[] = fakePosts;

    httpClientSpy.get.and.returnValue(of(expectedPosts));

    service.getAllPosts().subscribe(
      res => expect(res).toEqual(expectedPosts)
    );
    expect(httpClientSpy.get.calls.count()).toBe(1);
  });
});
